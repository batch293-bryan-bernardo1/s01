package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.Post;
import com.zuitt.wdc044.models.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface PostService {

    // create a post method
    void createPost(String stringToken, Post post);

    //getting all posts
    Iterable<Post> getPosts();


    //Edit a user's post method
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete a user's post method
    ResponseEntity deletePost(Long id, String stringToken);


    List myPosts(String stringToken);

}
