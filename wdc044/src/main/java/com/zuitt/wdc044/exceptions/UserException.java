package com.zuitt.wdc044.exceptions;

// No need to import because Exception Class is built-in
public class UserException extends Exception {

    public UserException(String message){
        super(message); // For inheritance
    }


}
