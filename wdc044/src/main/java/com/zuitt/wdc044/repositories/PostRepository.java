package com.zuitt.wdc044.repositories;

// Repositories
    // These are commonly used functionalities performed on databases objects.

import com.zuitt.wdc044.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

// An interface contains behavior that class implements.
// An interface is marked as @Repository which contains method for database manipulation.
// By extending CrudRepository class, the PostRepository has inherited its pre-defined methods for creating, retrieving, updating and deleting records.

@Repository
public interface PostRepository extends CrudRepository<Post, Object> {


}
